# Tyler's Sport Store (Not finished)

## About the project
This is a sample project that shows the components of ASP.Net Core MVC with Angular 9.1.1

## My Learning Objectives
* Putting together Angular and ASP.Net Core as one project.
* Utilizing Angular's core features as a front-end framework.
* Developing database models in C# and TypeScript with Entity Framework Core.
* Creating a connection string for SQL Server LocalDb to communicate with Entity Framework Core.
* Learning to export custom Angular modules into the application.
* Consuming and producing web services between Angular and ASP.Net Core.
* Applying Swagger to make sure that the test of the web service is accurate.
* Creating an Administration page to control what is in the store and not.
* Understanding best practices of Angular and ASP.Net Core.

## Packages used
* Microsoft.AspNetCore.Blazor.Server
* Microsoft.AspNetCore.JsonPatch
* Microsoft.AspNetCore.Mvc.NewtonsoftJson
* Microsoft.AspNetCore.SpaServices.Extensions
* Microsoft.EntityFrameworkCore.Design
* Microsoft.EntityFrameworkCore.SqlServer

