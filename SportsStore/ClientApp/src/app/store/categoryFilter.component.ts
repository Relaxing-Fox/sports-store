import { Component } from '@angular/core';
import { NavigationService } from '../models/navigation.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'store-categoryfilter',
    templateUrl: 'categoryFilter.component.html'
})
export class CategoryFilterComponent {
    constructor(public service: NavigationService) { }
}
