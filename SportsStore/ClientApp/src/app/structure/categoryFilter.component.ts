import { Component } from '@angular/core';
import { Repository } from '../models/repository';
import { NavigationService } from '../models/navigation.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'category-filter',
    templateUrl: 'categoryFilter.component.html'
})
export class CategoryFilterComponent {

    constructor(public service: NavigationService) {}

    // constructor(private repo: Repository) { }

    // setCategory(category: string) {
    //     this.repo.filter.category = category;
    //     this.repo.getProducts();
    // }
}
